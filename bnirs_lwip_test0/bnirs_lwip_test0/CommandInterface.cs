﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace bnirs_lwip_test0
{
    static class Constants
    {
        public const UInt32 RX_BUFFER_SIZE_BYTES = 1024 * 8;

    }

    enum RecState
    {
        REC_STATE_WAITING_FOR_SYNC_CODE_0_M = 0,
	    REC_STATE_WAITING_FOR_SYNC_CODE_1_M,
	    REC_STATE_WAITING_FOR_SYNC_CODE_2_M,
	    REC_STATE_WAITING_FOR_SYNC_CODE_3_M,
	    REC_STATE_WAITING_FOR_SIZE_0_M,
	    REC_STATE_WAITING_FOR_SIZE_1_M,
	    REC_STATE_WAITING_FOR_VERSION_M,
	    REC_STATE_WAITING_FOR_PACKET_TYPE_M,
	    REC_STATE_WAITING_FOR_CMH_COMPLETE_M,
	    REC_STATE_WAITING_FOR_PAYLOAD_COMPLETE_M
    };

    public class CommandInterface
    {
        RecState m_rsState;
        int m_nAuxRxCounter;
        Byte[] m_nAuxReceiveBuffer;
        BH_COMMON_HEADER m_echRcv = new BH_COMMON_HEADER();


        public CommandInterface()
        {
            m_nAuxReceiveBuffer = new byte[Constants.RX_BUFFER_SIZE_BYTES];
            m_rsState = RecState.REC_STATE_WAITING_FOR_SYNC_CODE_0_M;
            m_nAuxRxCounter = 0;
        }

        public void HandleMessage(Byte[] msg, int len_recvd)
        {
            int nCounter = 0;
            while (true)
            {
                if (nCounter >= len_recvd)
                    break;

                switch (m_rsState)
                {

                    case RecState.REC_STATE_WAITING_FOR_SIZE_0_M:
                        m_echRcv.nSizeBytes = (UInt16)msg[nCounter];
                        AddByteToAlignedBuffer(msg[nCounter]);
                        m_rsState = RecState.REC_STATE_WAITING_FOR_SIZE_1_M;
                        break;

                    case RecState.REC_STATE_WAITING_FOR_SIZE_1_M:
                        m_echRcv.nSizeBytes |= (ushort)(0xFF00 & ((UInt16)msg[nCounter] << 8));
                        AddByteToAlignedBuffer(msg[nCounter]);
                        m_rsState = RecState.REC_STATE_WAITING_FOR_VERSION_M;
                        break;

                    case RecState.REC_STATE_WAITING_FOR_VERSION_M:
                        m_echRcv.nVersion = msg[nCounter];
                        AddByteToAlignedBuffer(msg[nCounter]);
                        m_rsState = RecState.REC_STATE_WAITING_FOR_PACKET_TYPE_M;
                        break;

                    case RecState.REC_STATE_WAITING_FOR_PACKET_TYPE_M:
                        m_echRcv.nPacketType = msg[nCounter];
                        AddByteToAlignedBuffer(msg[nCounter]);
                        m_rsState = RecState.REC_STATE_WAITING_FOR_CMH_COMPLETE_M;
                        break;

                    case RecState.REC_STATE_WAITING_FOR_CMH_COMPLETE_M:
                        AddByteToAlignedBuffer(msg[nCounter]);

                        //if (m_nAuxRxCounter >= sizeof(BH_COMMON_HEADER_T))
                        //{				

                        //    m_rsState = RecState.REC_STATE_WAITING_FOR_PAYLOAD_COMPLETE_M;
                        //    break;
                        //}
                        m_rsState = RecState.REC_STATE_WAITING_FOR_PAYLOAD_COMPLETE_M;
                        break;

                    case RecState.REC_STATE_WAITING_FOR_PAYLOAD_COMPLETE_M:
                        AddByteToAlignedBuffer(msg[nCounter]);
                        Form1._Form1.updateTXT("Packet Recvd: Type: " + m_echRcv.nPacketType);

                        if (m_nAuxRxCounter >= m_echRcv.nSizeBytes)
                        {
                            // handle packet stuff here depending on type
                            
                        }
                        m_rsState = RecState.REC_STATE_WAITING_FOR_SYNC_CODE_0_M;
                        break;


                    case RecState.REC_STATE_WAITING_FOR_SYNC_CODE_1_M:
                        if (msg[nCounter] != (0xFF & (B2H_Constants.BH_SYNC_CODE) >> 8))
                        {
                            m_rsState = RecState.REC_STATE_WAITING_FOR_SYNC_CODE_0_M;
                            break;
                        }
                        AddByteToAlignedBuffer(msg[nCounter]);
                        m_rsState = RecState.REC_STATE_WAITING_FOR_SYNC_CODE_2_M;
                        break;

                    case RecState.REC_STATE_WAITING_FOR_SYNC_CODE_2_M:
                        if (msg[nCounter] != (0xFF & (B2H_Constants.BH_SYNC_CODE) >> 16))
                        {
                            m_rsState = RecState.REC_STATE_WAITING_FOR_SYNC_CODE_0_M;
                            break;
                        }
                        AddByteToAlignedBuffer(msg[nCounter]);
                        m_rsState = RecState.REC_STATE_WAITING_FOR_SYNC_CODE_3_M;
                        break;

                    case RecState.REC_STATE_WAITING_FOR_SYNC_CODE_3_M:
                        if (msg[nCounter] != (0xFF & (B2H_Constants.BH_SYNC_CODE) >> 24))
                        {
                            m_rsState = RecState.REC_STATE_WAITING_FOR_SYNC_CODE_0_M;
                            break;
                        }
                        AddByteToAlignedBuffer(msg[nCounter]);
                        m_rsState = RecState.REC_STATE_WAITING_FOR_SIZE_0_M;
                        break;

                    case RecState.REC_STATE_WAITING_FOR_SYNC_CODE_0_M:
                        m_nAuxRxCounter = 0;
                        if (msg[nCounter] != (0xFF & (B2H_Constants.BH_SYNC_CODE) >> 0))
                        {
                            m_rsState = RecState.REC_STATE_WAITING_FOR_SYNC_CODE_0_M;
                            break;
                        }
                        AddByteToAlignedBuffer(msg[nCounter]);
                        m_rsState = RecState.REC_STATE_WAITING_FOR_SYNC_CODE_1_M;
                        break;
                }
                nCounter++;


            }
        }

        public void AddByteToAlignedBuffer(byte b)
        {
            if (m_nAuxRxCounter < Constants.RX_BUFFER_SIZE_BYTES)
            {
                m_nAuxReceiveBuffer[m_nAuxRxCounter] = b;
                m_nAuxRxCounter++;
            }
        }


    }
}
