﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace bnirs_lwip_test0
{
    static class H2B_Constants
    {
        public const UInt32 HB_SYNC_CODE = 0x657989AB;
        public const Byte HB_PKT_TYPE_UNDEFINED = 0x00;
        public const Byte HB_PKT_TYPE_GET_STATUS = 0x01;
        public const Byte HB_PKT_TYPE_GET_DETECTOR_PROPERTIES = 0x02;
        public const Byte HB_PKT_TYPE_GET_SOURCE_PROPERTIES = 0x03;
        public const Byte HB_PKT_TYPE_SET_BIAS_DACS = 0x04;
        public const Byte HB_PKT_TYPE_SET_LED_I_DACS = 0x05;
        public const Byte HB_PKT_TYPE_SET_SOURCE_CONFIG = 0x06;
        public const Byte HB_PKT_TYPE_DAQ_CONFIG = 0x07;
        public const Byte HB_PKT_TYPE_ASSIGN_CHANNELS = 0x08;
        public const Byte HB_PKT_TYPE_START_STREAMING = 0x09;
        public const Byte HB_PKT_TYPE_STOP_STREAMING = 0x0A;
        public const Byte HB_PKT_TYPE_DUMMY_DATA_MODE = 0x0B;
    }

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public struct HB_COMMON_HEADER
    {
        public UInt32 nSync;
        public UInt16 nSizeBytes;
        public Byte nVersion;
        public Byte nPacketType;
        public UInt16 nPacketSequenceNumber;
        public UInt16 nCommandID;
        public UInt16 nSpare1;
        public UInt16 nSpare2;
    }

    class bnir_h2b
    {
    }
}
