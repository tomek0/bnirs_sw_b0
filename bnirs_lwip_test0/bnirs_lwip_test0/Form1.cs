﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace bnirs_lwip_test0
{
    public partial class Form1 : Form
    {
        Socket socket;
        static CommandInterface cmdint = new CommandInterface();
        public static Form1 _Form1;       

        public Form1()
        {
            InitializeComponent();
            _Form1 = this;
        }

        SocketAsyncEventArgs socketAsyncArgs;

        private void startReceive()
        {
            socketAsyncArgs = new SocketAsyncEventArgs();
            socketAsyncArgs.Completed += receiveCompleted;
            socketAsyncArgs.SetBuffer(new byte[1024], 0, 1024);
            if (!socket.ReceiveAsync(socketAsyncArgs)) { receiveCompleted(this, socketAsyncArgs); }
        }

        void receiveCompleted(object sender, SocketAsyncEventArgs e)
        {
            updateTXT(Encoding.Default.GetString(e.Buffer) + Environment.NewLine);
            if (!socket.ReceiveAsync(e)) { receiveCompleted(this, e); }
        }

        /* poor man's data binding, todo: update */
        public void updateTXT(string s)
        {
            this.BeginInvoke(new MethodInvoker(delegate
            {
                txtTCPIN.Text += Environment.NewLine + s;
                txtTCPIN.SelectionStart = txtTCPIN.Text.Length;
                txtTCPIN.ScrollToCaret();

            }));
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            int portRemote;

            if (socket != null && socket.Connected)
            {
                socketAsyncArgs.Completed -= receiveCompleted;
                socket.Disconnect(false);
            }
            else
            {
                try
                {
                    Int32.TryParse(txtPort.Text, out portRemote);

                    socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                    IPAddress ip = IPAddress.Parse(txtIP1.Text + "." + txtIP2.Text + "." + txtIP3.Text + "." + txtIP4.Text);
                    IPEndPoint remoteEP = new IPEndPoint(ip, portRemote);

                    socket.Connect(remoteEP);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                startReceive();
            }
            updateUIControls();
        }

        void updateUIControls()
        {
            if (socket != null && socket.Connected)
            {
                btnConnect.Text = "Disconnect";
                txtIP1.Enabled = txtIP2.Enabled = txtIP3.Enabled = txtIP4.Enabled = txtPort.Enabled = false;
                lblCon.Text = "Connected";

            }
            else
            {
                btnConnect.Text = "Connect";
                txtIP1.Enabled = txtIP2.Enabled = txtIP3.Enabled = txtIP4.Enabled = txtPort.Enabled = true;
                lblCon.Text = "Disconnected";
            }
        }

        private void btnTestStream_Click(object sender, EventArgs e)
        {
            // generate a dummy header to send
            HB_COMMON_HEADER dummy = new HB_COMMON_HEADER();
            dummy.nPacketType = H2B_Constants.HB_PKT_TYPE_START_STREAMING;
            dummy.nSync = H2B_Constants.HB_SYNC_CODE;
            dummy.nSizeBytes = 16;

            socket.Send(StructureToByteArray(dummy));
        }

        byte[] StructureToByteArray(object obj)
        {
            int len = Marshal.SizeOf(obj);
            byte[] arr = new byte[len];
            IntPtr ptr = Marshal.AllocHGlobal(len);
            Marshal.StructureToPtr(obj, ptr, true);
            Marshal.Copy(ptr, arr, 0, len);
            Marshal.FreeHGlobal(ptr);
            return arr;
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (socket != null && socket.Connected)
            {
                socketAsyncArgs.Completed -= receiveCompleted;
                socket.Disconnect(false);
            }
                
        }
    }
}
