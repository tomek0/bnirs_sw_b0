﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace bnirs_lwip_test0
{
    static class B2H_Constants
    {
        public const UInt32 BH_SYNC_CODE = 0x5678AF3E;

    }

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public struct BH_COMMON_HEADER
    {
        public UInt32 nSync;
        public UInt16 nSizeBytes;
        public Byte nVersion;
        public Byte nPacketType;
        public UInt16 nFirmwareVersion;
        public UInt16 nPacketSequenceNumber;
        public UInt16 nLastCommandID;
        public Int16 nLastError;
        public UInt16 nCurrent_mA;
        public UInt16 nVoltage_mV;
        public UInt16 nSpare1;
        public UInt16 nSpare2;
    }

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public struct BH_DATA_PKT_HEADER
    {
        public BH_COMMON_HEADER echResponse;
        public UInt32 nTimeStampTicksLSB;
        public UInt32 nTimeStampTicksMSB;
        public UInt16 nNumNIRSamples;
        public UInt16 nNumNIRChannels;
        public UInt16 nNumAMBSamples;
        public UInt16 nNumAMBChannels;
    }

    public class bnir_b2h
    {

    }
}
