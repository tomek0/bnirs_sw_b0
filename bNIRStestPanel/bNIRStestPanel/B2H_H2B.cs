﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace bNIRStestPanel
{
    public static class B2H
    {
        public const UInt32 BH_SYNC_CODE = 0x5678AF3E;

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        public struct BH_COMMON_HEADER
        {
            public UInt32 nSync;
            public UInt16 nSizeBytes;
            public Byte nVersion;
            public Byte nPacketType;
            public UInt16 nFirmwareVersion;
            public UInt16 nPacketSequenceNumber;
            public UInt16 nLastCommandID;
            public Int16 nLastError;
            public UInt16 nCurrent_mA;
            public UInt16 nVoltage_mV;
            public UInt16 nSpare1;
            public UInt16 nSpare2;
        }

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        public struct BH_DATA_PKT_HEADER
        {
            public BH_COMMON_HEADER echResponse;
            public UInt32 nTimeStampTicksLSB;
            public UInt32 nTimeStampTicksMSB;
            public UInt16 nNumNIRSamples;
            public UInt16 nNumNIRChannels;
            public UInt16 nNumAMBSamples;
            public UInt16 nNumAMBChannels;
        }

    }

    public static class H2B
    {
        public const UInt32 HB_SYNC_CODE = 0x657989AB;
        public const Byte HB_PKT_TYPE_UNDEFINED = 0x00;
        public const Byte HB_PKT_TYPE_GET_STATUS = 0x01;
        public const Byte HB_PKT_TYPE_GET_DETECTOR_PROPERTIES = 0x02;
        public const Byte HB_PKT_TYPE_GET_SOURCE_PROPERTIES = 0x03;
        public const Byte HB_PKT_TYPE_SET_BIAS_DACS = 0x04;
        public const Byte HB_PKT_TYPE_SET_LED_I_DACS = 0x05;
        public const Byte HB_PKT_TYPE_SET_SOURCE_CONFIG = 0x06;
        public const Byte HB_PKT_TYPE_DAQ_CONFIG = 0x07;
        public const Byte HB_PKT_TYPE_ASSIGN_CHANNELS = 0x08;
        public const Byte HB_PKT_TYPE_START_STREAMING = 0x09;
        public const Byte HB_PKT_TYPE_STOP_STREAMING = 0x0A;
        public const Byte HB_PKT_TYPE_DUMMY_DATA_MODE = 0x0B;
        public const Byte HB_PKT_TYPE_SET_STREAM_TICKS = 0x0C;

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        public struct HB_COMMON_HEADER
        {
            public UInt32 nSync;
            public UInt16 nSizeBytes;
            public Byte nVersion;
            public Byte nPacketType;
            public UInt16 nPacketSequenceNumber;
            public UInt16 nCommandID;
            public UInt16 nSpare1;
            public UInt16 nSpare2;
        }

        public static string[] HB_TXPKTS = new string[] {
            "HB_PKT_UNDEFINED",
            "HB_PKT_GET_STATUS",
            "HB_PKT_GET_DETECTOR_PR",
            "HB_PKT_GET_SOURCE_PROP",
            "HB_PKT_SET_BIAS_DACS",
            "HB_PKT_SET_LED_I_DACS",
            "HB_PKT_SET_SOURCE_CONF",
            "HB_PKT_DAQ_CONFIG",
            "HB_PKT_ASSIGN_CHANNELS",
            "HB_PKT_START_STREAMING",
            "HB_PKT_STOP_STREAMING",
            "HB_PKT_DUMMY_DATA_MODE",
            "HB_PKT_SET_STREAM_TICKS",
        };


        public static HB_CMD cmdCommonHeader = new HB_CMD("HB_COMMON_HEADER");

        static HB_CMD cmdSourceConfig = new HB_CMD("SET_BIAS_DACS");
        static HB_CMD cmdLEDsetIDACS = new HB_CMD("SET_LED_I_DACS");
        static HB_CMD cmdSrcConf = new HB_CMD("SET_SOURCE_CONF");

        public static List<HB_CMD> lstCommands = new List<HB_CMD>()
        {
            cmdSourceConfig,
            cmdLEDsetIDACS,
            cmdSrcConf,
        };

        public static int COMMON_HDR_SZ { get; private set; }

        public static void initCmds()
        {
            cmdCommonHeader.Add(new HB_FIELD("nSync", 32, 0x657989AB));
            cmdCommonHeader.Add(new HB_FIELD("nSizeBytes", 16, 16));
            cmdCommonHeader.Add(new HB_FIELD("nVersion", 8, 0));
            cmdCommonHeader.Add(new HB_FIELD("nPacketType", 8, 0));
            cmdCommonHeader.Add(new HB_FIELD("nCommandID", 16, 0));
            cmdCommonHeader.Add(new HB_FIELD("nSpare1", 16, 0));
            cmdCommonHeader.Add(new HB_FIELD("nSpare2", 16, 0));
            COMMON_HDR_SZ = 16;

            cmdSourceConfig.Add(new HB_FIELD("nNumSources", 16, 0));

            cmdLEDsetIDACS.Add(new HB_FIELD("nNumPasses", 16, 0));
            cmdLEDsetIDACS.Add(new HB_FIELD("nNumChannels", 8, 0));
            cmdLEDsetIDACS.Add(new HB_FIELD("nSpare1", 8, 0));

            cmdSrcConf.Add(new HB_FIELD("nModAmpuA", 32, 0));
            cmdSrcConf.Add(new HB_FIELD("nModFrqmHz", 32, 0));

        }


    }
 

    public class HB_CMD : List<HB_FIELD>
    {
        public string Name { get; set; }
        public HB_CMD(string name) { this.Name = name; }
        public List<HB_FIELD> Items {  get { return this; } }

    }

    public class HB_FIELD
    {
        public string Field { get; set; }
        public int Bits { get; set; }
        public UInt32 Val { get; set; }

        public HB_FIELD(string _property, int _size_bits, UInt32 _val)
        {
            Field = _property;
            Bits = _size_bits;
            Val = _val;

        }
    }

    public class H2B_TX
    {

        static int idxPacketType = 3;
        static int idxSize = 1;

        static List<H2B_TX> H2Btx_COMMON_HEADER = new List<H2B_TX>
        {
            new H2B_TX("nSync", 32, H2B.HB_SYNC_CODE),
            new H2B_TX("nSizeBytes", 16),
            new H2B_TX("nVersion", 8),
            new H2B_TX("nPacketType", 8),
            new H2B_TX("nPacketSequenc", 16),
            new H2B_TX("nCommandID", 16),
            new H2B_TX("nSpare2", 16),
            new H2B_TX("nSpare3", 16),

        };

        static List<H2B_TX> H2Btx_SET_SOURCE_CONF = new List<H2B_TX>
        {
            new H2B_TX("modAmp uA", 32),
            new H2B_TX("modFrq mHz", 32),

        };

        static List<H2B_TX> H2Btx_SET_BIAS_DACS = new List<H2B_TX>
        {
            new H2B_TX("numPasses", 16),
            new H2B_TX("numChannels", 8),
            new H2B_TX("enableAutoBias", 1),
            new H2B_TX("spare1", 7),

        };

        static List<H2B_TX> H2Btx_SET_DAQ_CONFIG = new List<H2B_TX>
        {
            new H2B_TX("nDAQMode", 16),
            new H2B_TX("enLoopback", 1),
            new H2B_TX("sampTickInt", 32),
            new H2B_TX("sampPpkt", 8),

        };

        static List<H2B_TX> H2Btx_SET_LED_I_DACS = new List<H2B_TX>
        {
            new H2B_TX("numPasses", 16),
            new H2B_TX("numChannels", 8),
            new H2B_TX("spare1", 8),

        };

        static List<H2B_TX> H2Btx_SET_STREAM_TICKS = new List<H2B_TX>
        {
            new H2B_TX("ticks", 16, 10),
        };


        public static BindingList<List<H2B_TX>> lstCommands = new BindingList<List<H2B_TX>>();

        public static void InitCmds()
        {

            lstCommands.Add(H2Btx_COMMON_HEADER);                                            // HB_PKT_UNDEFINED
            lstCommands.Add(H2Btx_COMMON_HEADER);                                            // HB_PKT_GET_STATUS
            lstCommands.Add(H2Btx_COMMON_HEADER);                                            // HB_PKT_GET_DETECTOR_PR
            lstCommands.Add(H2Btx_COMMON_HEADER);                                            // HB_PKT_GET_SOURCE_PROP
            lstCommands.Add(H2Btx_COMMON_HEADER.Concat(H2Btx_SET_BIAS_DACS).ToList());       // HB_PKT_SET_BIAS_DACS
            lstCommands.Add(H2Btx_COMMON_HEADER.Concat(H2Btx_SET_LED_I_DACS).ToList());      // HB_PKT_SET_LED_I_DACS
            lstCommands.Add(H2Btx_COMMON_HEADER.Concat(H2Btx_SET_SOURCE_CONF).ToList());     // HB_PKT_SET_SOURCE_CONF
            lstCommands.Add(H2Btx_COMMON_HEADER.Concat(H2Btx_SET_DAQ_CONFIG).ToList());      // HB_PKT_DAQ_CONFIG
            lstCommands.Add(H2Btx_COMMON_HEADER);                                            // HB_PKT_ASSIGN_CHANNELS
            lstCommands.Add(H2Btx_COMMON_HEADER);                                            // HB_PKT_START_STREAMING
            lstCommands.Add(H2Btx_COMMON_HEADER);                                            // HB_PKT_STOP_STREAMING
            lstCommands.Add(H2Btx_COMMON_HEADER);                                            // HB_PKT_DUMMY_DATA_MODE
            lstCommands.Add(H2Btx_COMMON_HEADER.Concat(H2Btx_SET_STREAM_TICKS).ToList());                                            // HB_PKT_DUMMY_DATA_MODE


            uint ptype = 0;
            foreach (List<H2B_TX> lstcmd in lstCommands)
            {
                int sz = 0;
                foreach (H2B_TX cmd in lstcmd)
                    sz += cmd.Bits;
                lstcmd[idxSize].Val = (uint)sz / 8;
                lstcmd[idxPacketType].Val = ptype;
                ptype++;
                
            }

        }
        
        public string Field { get; set; }
        public int Bits { get; set; }
        public UInt32 Val { get; set; }

        public H2B_TX(string _property, int _size_bits, UInt32 Val = 0 )
        {
            Field = _property;
            Bits = _size_bits;
            Val = Val;

        }

    }


    class B2H_H2B
    {
    }
}
