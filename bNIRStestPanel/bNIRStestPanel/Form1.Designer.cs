﻿namespace bNIRStestPanel
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.grpIP = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.panelSerialStatus = new System.Windows.Forms.SplitContainer();
            this.label5 = new System.Windows.Forms.Label();
            this.lblSerialStatus = new System.Windows.Forms.Label();
            this.cmbSerialPorts = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnSerialConnect = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panelTCPStatus = new System.Windows.Forms.SplitContainer();
            this.label4 = new System.Windows.Forms.Label();
            this.lblTCPStatus = new System.Windows.Forms.Label();
            this.btnIPConnect = new System.Windows.Forms.Button();
            this.txtIP0 = new System.Windows.Forms.TextBox();
            this.txtIP1 = new System.Windows.Forms.TextBox();
            this.txtIP2 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtIP3 = new System.Windows.Forms.TextBox();
            this.txtIPPort = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtSerial = new System.Windows.Forms.RichTextBox();
            this.btnSendPacket = new System.Windows.Forms.Button();
            this.cmbSendIPPacket = new System.Windows.Forms.ComboBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.dgTXComm = new System.Windows.Forms.DataGridView();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblKbPS = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.lblRxPktPS = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblRXPktSize = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lblRXHeaders = new System.Windows.Forms.Label();
            this.dgTXHdrCommon = new System.Windows.Forms.DataGridView();
            this.dgTXHdr = new System.Windows.Forms.DataGridView();
            this.cmbTXCmd = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lblMbPSec = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.grpIP.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelSerialStatus)).BeginInit();
            this.panelSerialStatus.Panel1.SuspendLayout();
            this.panelSerialStatus.Panel2.SuspendLayout();
            this.panelSerialStatus.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelTCPStatus)).BeginInit();
            this.panelTCPStatus.Panel1.SuspendLayout();
            this.panelTCPStatus.Panel2.SuspendLayout();
            this.panelTCPStatus.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgTXComm)).BeginInit();
            this.groupBox6.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgTXHdrCommon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgTXHdr)).BeginInit();
            this.SuspendLayout();
            // 
            // grpIP
            // 
            this.grpIP.Controls.Add(this.panel1);
            this.grpIP.Controls.Add(this.groupBox2);
            this.grpIP.Controls.Add(this.groupBox1);
            this.grpIP.Location = new System.Drawing.Point(12, 12);
            this.grpIP.Name = "grpIP";
            this.grpIP.Size = new System.Drawing.Size(381, 450);
            this.grpIP.TabIndex = 0;
            this.grpIP.TabStop = false;
            this.grpIP.Text = "Connect";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.panelSerialStatus);
            this.groupBox2.Controls.Add(this.cmbSerialPorts);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.btnSerialConnect);
            this.groupBox2.Location = new System.Drawing.Point(18, 179);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(353, 131);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Debug (Serial)";
            // 
            // panelSerialStatus
            // 
            this.panelSerialStatus.Location = new System.Drawing.Point(85, 89);
            this.panelSerialStatus.Name = "panelSerialStatus";
            // 
            // panelSerialStatus.Panel1
            // 
            this.panelSerialStatus.Panel1.Controls.Add(this.label5);
            // 
            // panelSerialStatus.Panel2
            // 
            this.panelSerialStatus.Panel2.BackColor = System.Drawing.Color.LightGray;
            this.panelSerialStatus.Panel2.Controls.Add(this.lblSerialStatus);
            this.panelSerialStatus.Size = new System.Drawing.Size(262, 28);
            this.panelSerialStatus.SplitterDistance = 86;
            this.panelSerialStatus.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(27, 5);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(48, 17);
            this.label5.TabIndex = 1;
            this.label5.Text = "Status";
            // 
            // lblSerialStatus
            // 
            this.lblSerialStatus.AutoSize = true;
            this.lblSerialStatus.Font = new System.Drawing.Font("Courier New", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSerialStatus.Location = new System.Drawing.Point(40, 5);
            this.lblSerialStatus.Name = "lblSerialStatus";
            this.lblSerialStatus.Size = new System.Drawing.Size(104, 17);
            this.lblSerialStatus.TabIndex = 2;
            this.lblSerialStatus.Text = "Disconnected";
            // 
            // cmbSerialPorts
            // 
            this.cmbSerialPorts.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSerialPorts.FormattingEnabled = true;
            this.cmbSerialPorts.Location = new System.Drawing.Point(12, 54);
            this.cmbSerialPorts.Name = "cmbSerialPorts";
            this.cmbSerialPorts.Size = new System.Drawing.Size(86, 24);
            this.cmbSerialPorts.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 17);
            this.label3.TabIndex = 8;
            this.label3.Text = "COM Port";
            // 
            // btnSerialConnect
            // 
            this.btnSerialConnect.Location = new System.Drawing.Point(221, 54);
            this.btnSerialConnect.Name = "btnSerialConnect";
            this.btnSerialConnect.Size = new System.Drawing.Size(108, 23);
            this.btnSerialConnect.TabIndex = 9;
            this.btnSerialConnect.Text = "Connect";
            this.btnSerialConnect.UseVisualStyleBackColor = true;
            this.btnSerialConnect.Click += new System.EventHandler(this.btnSerialConnect_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.panelTCPStatus);
            this.groupBox1.Controls.Add(this.btnIPConnect);
            this.groupBox1.Controls.Add(this.txtIP0);
            this.groupBox1.Controls.Add(this.txtIP1);
            this.groupBox1.Controls.Add(this.txtIP2);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtIP3);
            this.groupBox1.Controls.Add(this.txtIPPort);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(18, 35);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(350, 138);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "TCP (Socket)";
            // 
            // panelTCPStatus
            // 
            this.panelTCPStatus.Location = new System.Drawing.Point(85, 99);
            this.panelTCPStatus.Margin = new System.Windows.Forms.Padding(0);
            this.panelTCPStatus.Name = "panelTCPStatus";
            // 
            // panelTCPStatus.Panel1
            // 
            this.panelTCPStatus.Panel1.Controls.Add(this.label4);
            // 
            // panelTCPStatus.Panel2
            // 
            this.panelTCPStatus.Panel2.BackColor = System.Drawing.Color.LightGray;
            this.panelTCPStatus.Panel2.Controls.Add(this.lblTCPStatus);
            this.panelTCPStatus.Size = new System.Drawing.Size(264, 28);
            this.panelTCPStatus.SplitterDistance = 87;
            this.panelTCPStatus.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(24, 5);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(48, 17);
            this.label4.TabIndex = 1;
            this.label4.Text = "Status";
            // 
            // lblTCPStatus
            // 
            this.lblTCPStatus.AutoSize = true;
            this.lblTCPStatus.Font = new System.Drawing.Font("Courier New", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTCPStatus.Location = new System.Drawing.Point(39, 5);
            this.lblTCPStatus.Name = "lblTCPStatus";
            this.lblTCPStatus.Size = new System.Drawing.Size(104, 17);
            this.lblTCPStatus.TabIndex = 2;
            this.lblTCPStatus.Text = "Disconnected";
            // 
            // btnIPConnect
            // 
            this.btnIPConnect.Location = new System.Drawing.Point(218, 51);
            this.btnIPConnect.Name = "btnIPConnect";
            this.btnIPConnect.Size = new System.Drawing.Size(108, 23);
            this.btnIPConnect.TabIndex = 10;
            this.btnIPConnect.Text = "Connect";
            this.btnIPConnect.UseVisualStyleBackColor = true;
            this.btnIPConnect.Click += new System.EventHandler(this.btnIPConnect_Click);
            // 
            // txtIP0
            // 
            this.txtIP0.Location = new System.Drawing.Point(12, 52);
            this.txtIP0.MaxLength = 3;
            this.txtIP0.Name = "txtIP0";
            this.txtIP0.Size = new System.Drawing.Size(40, 22);
            this.txtIP0.TabIndex = 0;
            this.txtIP0.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtIP1
            // 
            this.txtIP1.Location = new System.Drawing.Point(58, 52);
            this.txtIP1.MaxLength = 3;
            this.txtIP1.Name = "txtIP1";
            this.txtIP1.Size = new System.Drawing.Size(40, 22);
            this.txtIP1.TabIndex = 1;
            this.txtIP1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtIP2
            // 
            this.txtIP2.Location = new System.Drawing.Point(104, 52);
            this.txtIP2.MaxLength = 3;
            this.txtIP2.Name = "txtIP2";
            this.txtIP2.Size = new System.Drawing.Size(40, 22);
            this.txtIP2.TabIndex = 2;
            this.txtIP2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 81);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 17);
            this.label2.TabIndex = 7;
            this.label2.Text = "Port";
            // 
            // txtIP3
            // 
            this.txtIP3.Location = new System.Drawing.Point(150, 52);
            this.txtIP3.MaxLength = 3;
            this.txtIP3.Name = "txtIP3";
            this.txtIP3.Size = new System.Drawing.Size(40, 22);
            this.txtIP3.TabIndex = 3;
            this.txtIP3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtIPPort
            // 
            this.txtIPPort.Location = new System.Drawing.Point(12, 103);
            this.txtIPPort.MaxLength = 3;
            this.txtIPPort.Name = "txtIPPort";
            this.txtIPPort.Size = new System.Drawing.Size(50, 22);
            this.txtIPPort.TabIndex = 4;
            this.txtIPPort.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 17);
            this.label1.TabIndex = 6;
            this.label1.Text = "IP Address";
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox3.Controls.Add(this.txtSerial);
            this.groupBox3.Location = new System.Drawing.Point(12, 468);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(683, 281);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Serial Debug Console";
            // 
            // txtSerial
            // 
            this.txtSerial.BackColor = System.Drawing.Color.Black;
            this.txtSerial.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtSerial.Font = new System.Drawing.Font("Courier New", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSerial.ForeColor = System.Drawing.Color.GreenYellow;
            this.txtSerial.Location = new System.Drawing.Point(3, 18);
            this.txtSerial.Name = "txtSerial";
            this.txtSerial.ReadOnly = true;
            this.txtSerial.Size = new System.Drawing.Size(677, 260);
            this.txtSerial.TabIndex = 0;
            this.txtSerial.Text = "";
            // 
            // btnSendPacket
            // 
            this.btnSendPacket.Location = new System.Drawing.Point(84, 325);
            this.btnSendPacket.Name = "btnSendPacket";
            this.btnSendPacket.Size = new System.Drawing.Size(113, 23);
            this.btnSendPacket.TabIndex = 2;
            this.btnSendPacket.Text = "Send Packet";
            this.btnSendPacket.UseVisualStyleBackColor = true;
            this.btnSendPacket.Click += new System.EventHandler(this.btnSendPacket_Click);
            // 
            // cmbSendIPPacket
            // 
            this.cmbSendIPPacket.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSendIPPacket.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbSendIPPacket.FormattingEnabled = true;
            this.cmbSendIPPacket.Location = new System.Drawing.Point(13, 29);
            this.cmbSendIPPacket.Name = "cmbSendIPPacket";
            this.cmbSendIPPacket.Size = new System.Drawing.Size(271, 23);
            this.cmbSendIPPacket.TabIndex = 3;
            this.cmbSendIPPacket.SelectedIndexChanged += new System.EventHandler(this.cmbSendIPPacket_SelectedIndexChanged);
            // 
            // groupBox4
            // 
            this.groupBox4.Location = new System.Drawing.Point(701, 428);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(508, 321);
            this.groupBox4.TabIndex = 4;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "IP Receive";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.dgTXComm);
            this.groupBox5.Controls.Add(this.btnSendPacket);
            this.groupBox5.Controls.Add(this.cmbSendIPPacket);
            this.groupBox5.Location = new System.Drawing.Point(408, 12);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(287, 450);
            this.groupBox5.TabIndex = 5;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "IP Transmit";
            // 
            // dgTXComm
            // 
            this.dgTXComm.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgTXComm.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgTXComm.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgTXComm.Location = new System.Drawing.Point(13, 65);
            this.dgTXComm.Name = "dgTXComm";
            this.dgTXComm.RowHeadersVisible = false;
            this.dgTXComm.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgTXComm.RowTemplate.Height = 20;
            this.dgTXComm.Size = new System.Drawing.Size(271, 245);
            this.dgTXComm.TabIndex = 4;
            this.dgTXComm.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgTXPkt_CellContentClick);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.dgTXHdrCommon);
            this.groupBox6.Controls.Add(this.label9);
            this.groupBox6.Controls.Add(this.cmbTXCmd);
            this.groupBox6.Controls.Add(this.label7);
            this.groupBox6.Controls.Add(this.dgTXHdr);
            this.groupBox6.Location = new System.Drawing.Point(702, 23);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(503, 723);
            this.groupBox6.TabIndex = 6;
            this.groupBox6.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lblMbPSec);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.lblKbPS);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.lblRxPktPS);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.lblRXPktSize);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.lblRXHeaders);
            this.panel1.Location = new System.Drawing.Point(8, 323);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(170, 108);
            this.panel1.TabIndex = 7;
            // 
            // lblKbPS
            // 
            this.lblKbPS.AutoSize = true;
            this.lblKbPS.Location = new System.Drawing.Point(15, 59);
            this.lblKbPS.Name = "lblKbPS";
            this.lblKbPS.Size = new System.Drawing.Size(16, 17);
            this.lblKbPS.TabIndex = 9;
            this.lblKbPS.Text = "0";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(65, 59);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(37, 17);
            this.label11.TabIndex = 8;
            this.label11.Text = "KB/s";
            // 
            // lblRxPktPS
            // 
            this.lblRxPktPS.AutoSize = true;
            this.lblRxPktPS.Location = new System.Drawing.Point(15, 40);
            this.lblRxPktPS.Name = "lblRxPktPS";
            this.lblRxPktPS.Size = new System.Drawing.Size(16, 17);
            this.lblRxPktPS.TabIndex = 7;
            this.lblRxPktPS.Text = "0";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(65, 40);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(39, 17);
            this.label10.TabIndex = 6;
            this.label10.Text = "Pkt/s";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(65, 2);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(66, 17);
            this.label6.TabIndex = 0;
            this.label6.Text = "Pkts RXd";
            // 
            // lblRXPktSize
            // 
            this.lblRXPktSize.AutoSize = true;
            this.lblRXPktSize.Location = new System.Drawing.Point(15, 21);
            this.lblRXPktSize.Name = "lblRXPktSize";
            this.lblRXPktSize.Size = new System.Drawing.Size(16, 17);
            this.lblRXPktSize.TabIndex = 5;
            this.lblRXPktSize.Text = "0";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(65, 21);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(78, 17);
            this.label8.TabIndex = 4;
            this.label8.Text = "PktSize (B)";
            // 
            // lblRXHeaders
            // 
            this.lblRXHeaders.AutoSize = true;
            this.lblRXHeaders.Location = new System.Drawing.Point(15, 2);
            this.lblRXHeaders.Name = "lblRXHeaders";
            this.lblRXHeaders.Size = new System.Drawing.Size(16, 17);
            this.lblRXHeaders.TabIndex = 2;
            this.lblRXHeaders.Text = "0";
            // 
            // dgTXHdrCommon
            // 
            this.dgTXHdrCommon.AllowUserToResizeColumns = false;
            this.dgTXHdrCommon.AllowUserToResizeRows = false;
            this.dgTXHdrCommon.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgTXHdrCommon.Location = new System.Drawing.Point(17, 55);
            this.dgTXHdrCommon.Name = "dgTXHdrCommon";
            this.dgTXHdrCommon.RowHeadersVisible = false;
            this.dgTXHdrCommon.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgTXHdrCommon.RowTemplate.Height = 20;
            this.dgTXHdrCommon.Size = new System.Drawing.Size(242, 169);
            this.dgTXHdrCommon.TabIndex = 0;
            // 
            // dgTXHdr
            // 
            this.dgTXHdr.AllowUserToResizeColumns = false;
            this.dgTXHdr.AllowUserToResizeRows = false;
            this.dgTXHdr.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgTXHdr.Location = new System.Drawing.Point(17, 289);
            this.dgTXHdr.Name = "dgTXHdr";
            this.dgTXHdr.RowHeadersVisible = false;
            this.dgTXHdr.RowTemplate.Height = 24;
            this.dgTXHdr.Size = new System.Drawing.Size(242, 131);
            this.dgTXHdr.TabIndex = 1;
            // 
            // cmbTXCmd
            // 
            this.cmbTXCmd.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTXCmd.FormattingEnabled = true;
            this.cmbTXCmd.Location = new System.Drawing.Point(17, 255);
            this.cmbTXCmd.Name = "cmbTXCmd";
            this.cmbTXCmd.Size = new System.Drawing.Size(163, 24);
            this.cmbTXCmd.TabIndex = 2;
            this.cmbTXCmd.SelectedIndexChanged += new System.EventHandler(this.cmbTXCmd_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(14, 24);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(136, 17);
            this.label7.TabIndex = 3;
            this.label7.Text = "TX Common Header";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(14, 229);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(87, 17);
            this.label9.TabIndex = 3;
            this.label9.Text = "Packet Type";
            // 
            // lblMbPSec
            // 
            this.lblMbPSec.AutoSize = true;
            this.lblMbPSec.Location = new System.Drawing.Point(15, 77);
            this.lblMbPSec.Name = "lblMbPSec";
            this.lblMbPSec.Size = new System.Drawing.Size(16, 17);
            this.lblMbPSec.TabIndex = 11;
            this.lblMbPSec.Text = "0";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(65, 77);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(42, 17);
            this.label13.TabIndex = 10;
            this.label13.Text = "Mbps";
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1217, 756);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.grpIP);
            this.Name = "frmMain";
            this.Text = "bNIRStestPanel";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMain_FormClosing);
            this.grpIP.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.panelSerialStatus.Panel1.ResumeLayout(false);
            this.panelSerialStatus.Panel1.PerformLayout();
            this.panelSerialStatus.Panel2.ResumeLayout(false);
            this.panelSerialStatus.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelSerialStatus)).EndInit();
            this.panelSerialStatus.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panelTCPStatus.Panel1.ResumeLayout(false);
            this.panelTCPStatus.Panel1.PerformLayout();
            this.panelTCPStatus.Panel2.ResumeLayout(false);
            this.panelTCPStatus.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelTCPStatus)).EndInit();
            this.panelTCPStatus.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgTXComm)).EndInit();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgTXHdrCommon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgTXHdr)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpIP;
        private System.Windows.Forms.TextBox txtIPPort;
        private System.Windows.Forms.TextBox txtIP3;
        private System.Windows.Forms.TextBox txtIP2;
        private System.Windows.Forms.TextBox txtIP1;
        private System.Windows.Forms.TextBox txtIP0;
        private System.Windows.Forms.ComboBox cmbSerialPorts;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnIPConnect;
        private System.Windows.Forms.Button btnSerialConnect;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.SplitContainer panelSerialStatus;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblSerialStatus;
        private System.Windows.Forms.SplitContainer panelTCPStatus;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblTCPStatus;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RichTextBox txtSerial;
        private System.Windows.Forms.Button btnSendPacket;
        private System.Windows.Forms.ComboBox cmbSendIPPacket;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.DataGridView dgTXComm;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblKbPS;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label lblRxPktPS;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblRXPktSize;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblRXHeaders;
        private System.Windows.Forms.DataGridView dgTXHdrCommon;
        private System.Windows.Forms.DataGridView dgTXHdr;
        private System.Windows.Forms.ComboBox cmbTXCmd;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lblMbPSec;
        private System.Windows.Forms.Label label13;
    }
}

