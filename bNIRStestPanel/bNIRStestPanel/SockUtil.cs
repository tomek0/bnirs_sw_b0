﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace bNIRStestPanel
{
    class SockUtil
    {
        Socket socket;
        SocketAsyncEventArgs socketAsyncArgs;
        IPAddress ip;
        IPEndPoint remoteEP;
        int port;
        Action<byte[], int> ReceiveCallback;

        public SockUtil(string _ip, string _port, Action<byte[], int> _rx_callback)
        {
            if (!IPAddress.TryParse(_ip, out ip))
                throw new Exception("Error parsing IP address");

            if (!int.TryParse(_port, out port) && port > 65536 ) 
                throw new Exception("Error parsing IP port");

            
            remoteEP = new IPEndPoint(ip, port);
            ReceiveCallback = _rx_callback;

        }

        public void Connect()
        {
            socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            socketAsyncArgs = new SocketAsyncEventArgs();
            socketAsyncArgs.SetBuffer(new byte[2048], 0, 2048);
            socketAsyncArgs.Completed += receiveCompleted;
            socketAsyncArgs.RemoteEndPoint = remoteEP;

            Application.UseWaitCursor = true;
            Cursor.Current = Cursors.WaitCursor;
            IAsyncResult result = socket.BeginConnect(ip, port, null, null);
            bool success = result.AsyncWaitHandle.WaitOne(3000, true);
            
            if (!socket.Connected)
            {
                
                socket.Close();     // must close the socket here
                Application.UseWaitCursor = false;
                Cursor.Current = Cursors.Default;
                throw new ApplicationException("Failed to connect server.");
            }

            Application.UseWaitCursor = false;
            Cursor.Current = Cursors.Default;
            
            if (!socket.ReceiveAsync(socketAsyncArgs))
                receiveCompleted(this, socketAsyncArgs);
        }

        public bool isConnected()
        {
            if (socket == null)
                return false;
            return socket.Connected;
        }

        private void startReceive()
        {
            
        }

        void receiveCompleted(object sender, SocketAsyncEventArgs e)
        {
            ReceiveCallback.Invoke(e.Buffer, e.BytesTransferred);
            if (!socket.ReceiveAsync(socketAsyncArgs))
                receiveCompleted(this, socketAsyncArgs);
        }

        public void Disconnect()
        {
            if (socket != null && socket.Connected)
            {
                socketAsyncArgs.Completed -= receiveCompleted;
                socket.Disconnect(false);
            }
        }

        public void SendObject(object obj)
        {
            socket.Send(StructureToByteArray(obj));
        }

        public void SendBytes(byte[] tosend)
        {
            socket.Send(StructureToByteArray(tosend));
        }

        byte[] StructureToByteArray(object obj)
        {
            int len = Marshal.SizeOf(obj);
            byte[] arr = new byte[len];
            IntPtr ptr = Marshal.AllocHGlobal(len);
            Marshal.StructureToPtr(obj, ptr, true);
            Marshal.Copy(ptr, arr, 0, len);
            Marshal.FreeHGlobal(ptr);
            return arr;
        }

    }
}
