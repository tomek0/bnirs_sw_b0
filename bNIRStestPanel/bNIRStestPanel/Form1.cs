﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace bNIRStestPanel
{
    

    public partial class frmMain : Form
    {
        Properties.Settings Settings = Properties.Settings.Default;
        SerialPort comport = new SerialPort();
        static CommandHandler cmdHandler = new CommandHandler();
        SockUtil sockutil;
        TextBox[] txtIP;
        Timer tm = new Timer();


        static string[] txtConnStatus = new string[] { "Disconnected", "Connected" };
        static string[] txtConnBtnStatus = new string[] { "Connect", "Disconnect" };
        static Color[] colorConnState = new Color[] { Color.LightGray, Color.LightGreen };

        public static frmMain _Form1;
        const int TIMER_MS = 50;
        static int uiTimeToSec = 0;


        public frmMain()
        {
            
            H2B_TX.InitCmds();
            InitializeComponent();
            H2B.initCmds();
            InitControls();
        }

        void InitControls()
        {
            txtIP = new TextBox[] { txtIP0, txtIP1, txtIP2, txtIP3 };

            for (int i = 0; i < txtIP.Length; i++)
            {
                txtIP[i].Text = Settings.ipaddr[i];
                txtIP[i].Click += TextBoxOnClick;
            }
            txtIPPort.Click += TextBoxOnClick;
            txtIPPort.Text = Settings.ipport;
            
            foreach (string p in SerialPort.GetPortNames())
            {
                cmbSerialPorts.Items.Add(p);
                if (p == Settings.comport)
                    cmbSerialPorts.SelectedItem = p;
            }
            
            if (Settings.comport.Length < 2 && cmbSerialPorts.Items.Count > 0)
                cmbSerialPorts.SelectedIndex = 0;

            foreach (string s in H2B.HB_TXPKTS)
                cmbSendIPPacket.Items.Add(s);
            cmbSendIPPacket.SelectedIndex = 9;

            comport.BaudRate = 115200;

            dgTXHdrCommon.DataSource = H2B.cmdCommonHeader;

            foreach (HB_CMD cmd in H2B.lstCommands)
                cmbTXCmd.Items.Add(cmd.Name);
            cmbTXCmd.SelectedIndex = 0;


            dgTXHdrCommon.DataBindingComplete += Dg_DataBindingComplete;
            dgTXHdr.DataBindingComplete += Dg_DataBindingComplete;
            dgTXComm.DataBindingComplete += Dg_DataBindingComplete;


            tm.Interval = TIMER_MS;
            tm.Tick += Tm_Tick;
            _Form1 = this;
            

        }

        private void Dg_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            var dataGridView = sender as DataGridView;
            if (dataGridView != null)
            {
                dataGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
                dataGridView.Columns[dataGridView.ColumnCount - 1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }
        }

        void updateUIState()
        {
            int comst = comport.IsOpen.ToInt();
            lblSerialStatus.Text = txtConnStatus[comst];
            btnSerialConnect.Text = txtConnBtnStatus[comst];
            panelSerialStatus.Panel2.BackColor = colorConnState[comst];


            if (sockutil != null)
            {
                int ipst = sockutil.isConnected().ToInt();
                lblTCPStatus.Text = txtConnStatus[ipst];
                btnIPConnect.Text = txtConnBtnStatus[ipst];
                panelTCPStatus.Panel2.BackColor = colorConnState[ipst];
            }
            
            
        }

        //public void tmpUpdateUI(uint _bytes, uint _pkts, uint _psize)
        //{
        //    BeginInvoke(new EventHandler(delegate
        //    {
        //        lblRXHeaders.Text = _pkts.ToString();
        //        lblRXKB.Text = (_bytes/8).ToString();
        //        lblRXPktSize.Text = _psize.ToString();

        //    }));
        //}

        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (comport.IsOpen)
                comport.Close();
            if (sockutil != null && sockutil.isConnected())
                sockutil.Disconnect();
            saveSettings();

        }

        private void saveSettings()
        {
            Settings.ipaddr.Clear();
            foreach (TextBox tb in txtIP)
                Settings.ipaddr.Add(tb.Text);
            Settings.ipport = txtIPPort.Text;
            Settings.comport = cmbSerialPorts.Text;
            Settings.Save();
        }

        private void btnSerialConnect_Click(object sender, EventArgs e)
        {
            if (!comport.IsOpen)
            {
                comport.PortName = cmbSerialPorts.Text;
                comport.DataReceived += SerialPort_DataReceived;
                comport.Open();

            }
            else
            {
                comport.Close();
            }
            updateUIState();
        }

        private void SerialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            string inData = comport.ReadExisting();
            displaySerialToWindow(inData);
        }

        private void displaySerialToWindow(string inData)
        {
            BeginInvoke(new EventHandler(delegate
            {
                txtSerial.AppendText(inData);
                txtSerial.ScrollToCaret();
            }));
        }

        private void TextBoxOnClick(object sender, EventArgs eventArgs)
        {
            var textBox = (TextBox)sender;
            textBox.SelectAll();
            textBox.Focus();
        }

        string GetIPString()
        {
            string tmpip = "";
            foreach (TextBox s in txtIP)
                tmpip += s.Text + ".";
            tmpip = tmpip.Remove(tmpip.Length - 1);
            return tmpip;
        }

        private void btnIPConnect_Click(object sender, EventArgs e)
        {
            if (sockutil == null || !sockutil.isConnected())
            {
                
                try
                {
                    sockutil = new SockUtil(GetIPString(), txtIPPort.Text, cmdHandler.HandleMessage);
                    lblTCPStatus.Text = btnIPConnect.Text = "Connecting...";
                    sockutil.Connect();
                }catch(Exception ex)
                {
                    updateUIState();
                    MessageBox.Show(ex.Message);
                }
                tm.Start();
            }
            else
            {
                sockutil.Disconnect();
                if (tm.Enabled)
                    tm.Stop();

            }
            updateUIState();
        }

        private void btnSendPacket_Click(object sender, EventArgs e)
        {

            
            if (sockutil != null && sockutil.isConnected())
            {
                H2B.HB_COMMON_HEADER hdr = new H2B.HB_COMMON_HEADER();
                hdr.nSync = H2B.HB_SYNC_CODE;
                hdr.nPacketType = (byte)cmbSendIPPacket.SelectedIndex;
                hdr.nSizeBytes = 16;

                if (cmbSendIPPacket.SelectedIndex == cmbSendIPPacket.Items.Count - 1)              
                    hdr.nSpare1 = (ushort)int.Parse(dgTXComm.Rows[8].Cells[2].Value.ToString());

                sockutil.SendObject(hdr);

            }
        }

        private void dgTXPkt_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void cmbSendIPPacket_SelectedIndexChanged(object sender, EventArgs e)
        {
            dgTXComm.DataSource = null;
            dgTXComm.DataSource = H2B_TX.lstCommands[cmbSendIPPacket.SelectedIndex];
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            
        }

 
        private void button1_Click_2(object sender, EventArgs e)
        {
           
            
        }

        public static uint uiPktsRX = 0;
        public static uint uiBytesRX = 0;
        public static uint uiPktSizeRX = 0;
        public static uint uiPkPScnt = 0;
        static uint uiLstPktCnt;
        static uint uiLstByteCnt;

        uint cntDeltaTRate = 1000;

        private void Tm_Tick(object sender, EventArgs e)
        {

            lblRXHeaders.Text = uiPktsRX.ToString();

            //lblRXKB.Text = ((int)((double)uiKBPktsRX/1000.0)).ToString();

            lblRXPktSize.Text = uiPktSizeRX.ToString();

            if (uiTimeToSec++ >= cntDeltaTRate / TIMER_MS)  // 1 sec reached
            {

                uint got_packets = uiPktsRX - uiLstPktCnt;
                lblRxPktPS.Text = (got_packets*(1000/cntDeltaTRate)).ToString();
                uint got_bytes = uiBytesRX - uiLstByteCnt;
                double kBps = (((double)got_bytes / cntDeltaTRate));
                lblKbPS.Text = kBps.ToString("n1");

                lblMbPSec.Text = ((kBps / 1000)*8).ToString("n1");
                uiLstPktCnt = uiPktsRX;
                uiLstByteCnt = uiBytesRX;
                uiTimeToSec = 0;
            }
        }

        private void cmbTXCmd_SelectedIndexChanged(object sender, EventArgs e)
        {
            uint sz = (uint)H2B.COMMON_HDR_SZ;
            foreach (HB_FIELD hbf in H2B.lstCommands[cmbTXCmd.SelectedIndex])
                sz += (uint)hbf.Bits;
            H2B.cmdCommonHeader[1].Val = sz;
            dgTXHdrCommon.Refresh();
            dgTXHdr.DataSource = H2B.lstCommands[cmbTXCmd.SelectedIndex];
        }

    }

    public static class BooleanExtensions
    {
        public static int ToInt(this bool value)
        {
            return value ? 1 : 0;
        }
    }
}
